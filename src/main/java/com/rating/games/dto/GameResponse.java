package com.rating.games.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GameResponse {
    private Integer id;
    private String name;
    private String description;
    private String image;
    private LocalDate dateLaunch;
}
