package com.rating.games.configuration;

import com.rating.games.handler.ErrorGeneric;
import com.rating.games.handler.GameException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Configuration
public class WebClientConfiguration {
    private WebClient webClient;

    public WebClient build() {
        if (Objects.isNull(this.webClient)) {
            this.webClient = WebClient.builder()
                    .filter(ExchangeFilterFunction.ofResponseProcessor(this::renderApiErrorResponse))
                    .baseUrl("http://localhost:8080")
                    .build();
        }
            return this.webClient;
    }

    private Mono<ClientResponse> renderApiErrorResponse(ClientResponse clientResponse) {
        if(clientResponse.statusCode().isError()){
            return clientResponse.bodyToMono(GameException.class)
                    .flatMap(apiErrorResponse -> Mono.error(new GameException(
                            apiErrorResponse.getMessage()
                    )));
        }
        return Mono.just(clientResponse);
    }

}
