package com.rating.games.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import reactor.core.publisher.Mono;

import java.util.TimeZone;

@ControllerAdvice
public class ExceptionHandlerDetails {

    @ExceptionHandler(GameException.class)
    public ResponseEntity<?> UserExceptionHandler(GameException exception) {
        var error =  Mono.just(ErrorGeneric.builder()
                .message(exception.getMessage())
                .code(400)
                .timeZone(TimeZone.getDefault())
                .build());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
