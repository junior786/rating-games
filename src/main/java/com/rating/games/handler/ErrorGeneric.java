package com.rating.games.handler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.TimeZone;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorGeneric {
    private String message;
    private TimeZone timeZone;
    private Integer code;
}
