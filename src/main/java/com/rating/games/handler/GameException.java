package com.rating.games.handler;

public class GameException extends RuntimeException {
    public GameException(String message) {
        super(message);
    }
}
