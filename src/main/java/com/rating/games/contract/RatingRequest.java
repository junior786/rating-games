package com.rating.games.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RatingRequest {
    private Integer id;
    private String message;
    private String name;
    private Integer stars;
    private Integer idGame;
}
