package com.rating.games.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table("rating")
public class RatingContract {
    private Integer id;
    private String message;
    private LocalDate date;
    private String name;
    private Integer stars;
    @Column("idgame")
    private Integer idgame;

}
