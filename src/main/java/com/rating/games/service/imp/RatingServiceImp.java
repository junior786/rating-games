package com.rating.games.service.imp;

import com.rating.games.configuration.WebClientConfiguration;
import com.rating.games.contract.RatingContract;
import com.rating.games.contract.RatingRequest;
import com.rating.games.dto.GameResponse;
import com.rating.games.dto.RatingResponse;
import com.rating.games.handler.GameException;
import com.rating.games.repository.RatingRepository;
import com.rating.games.service.contract.RatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class RatingServiceImp implements RatingService {
    private final WebClientConfiguration webClient;
    private final RatingRepository ratingRepository;

    @Override
    public Flux<RatingResponse> findByRatings(Integer idGame) {
        return this.ratingRepository.findAllByidgame(idGame)
                .map(it -> RatingResponse.builder()
                        .date(it.getDate())
                        .message(it.getMessage())
                        .name(it.getName())
                        .stars(it.getStars())
                        .build());
    }

    @Override
    public Flux<RatingResponse> saveRatingGame(RatingRequest ratingRequest) {
         return this.webClient.build().get().uri("/v1/game/" + ratingRequest.getIdGame())
                 .retrieve()
                 .bodyToMono(GameResponse.class)
                 .flatMapMany(it -> this.ratingRepository
                         .save(RatingContract.builder()
                                 .idgame(ratingRequest.getIdGame())
                                 .date(LocalDate.now())
                                 .message(ratingRequest.getMessage())
                                 .name(ratingRequest.getName())
                                 .stars(ratingRequest.getStars())
                                 .build()))
                 .flatMap(it -> this.ratingRepository.findAllByidgame(it.getIdgame()))
                 .map(it -> RatingResponse.builder()
                         .date(it.getDate())
                         .message(it.getMessage())
                         .name(it.getName())
                         .stars(it.getStars())
                         .build());
    }
}
