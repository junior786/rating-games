package com.rating.games.service.contract;

import com.rating.games.contract.RatingRequest;
import com.rating.games.dto.RatingResponse;
import reactor.core.publisher.Flux;

public interface RatingService {
    Flux<RatingResponse> findByRatings(Integer idGame);
    Flux<RatingResponse> saveRatingGame(RatingRequest ratingRequest);
}
