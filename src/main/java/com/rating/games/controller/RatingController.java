package com.rating.games.controller;

import com.rating.games.contract.RatingRequest;
import com.rating.games.dto.RatingResponse;
import com.rating.games.service.contract.RatingService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/rating/v1")
@AllArgsConstructor
public class RatingController {
    private final RatingService ratingService;

    @GetMapping("/{id}")
    public Flux<RatingResponse> getRatingById(@PathVariable Integer id) {
         return this.ratingService.findByRatings(id);
    }

    @PostMapping
    public Flux<RatingResponse> saveRating(@RequestBody RatingRequest getRating) {
        return this.ratingService.saveRatingGame(getRating);
    }
}
