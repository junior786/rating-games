package com.rating.games.repository;
import com.rating.games.contract.RatingContract;
import com.rating.games.dto.RatingResponse;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface RatingRepository extends ReactiveCrudRepository<RatingContract, Integer> {
    Flux<RatingContract> findAllByidgame(Integer id);
}
