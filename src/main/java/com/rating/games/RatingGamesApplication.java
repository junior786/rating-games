package com.rating.games;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatingGamesApplication {
	public static void main(String[] args) {
		SpringApplication.run(RatingGamesApplication.class, args);
	}
}
